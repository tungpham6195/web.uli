﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebULI.Startup))]
namespace WebULI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
