﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UM_Auth.CodeHelper;

namespace WebULI.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult Contact(string name, string email, string subject, string message)
        {
            try
            {
                // điều chỉnh lại nội dung
                MailHelper.SendMail("chuongnh.hcm@gmail.com", email, "[ULI] thông báo", "Bạn vừa đăng ký nhận thông báo của ULI.");

                return Json(new { status = true, message = email }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}