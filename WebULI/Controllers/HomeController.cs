﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM_Auth.CodeHelper;
using WebULI.Models;

namespace WebULI.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public async Task<ActionResult> Index()
        {
            ViewBag.Courses = await db.Courses.Where(x => x.ShowOnHome == true).ToListAsync();
            ViewBag.UpCommingClasses = await db.UpCommingClasses.Where(x => x.ShowOnHome == true).ToListAsync();
            return View();
        }

        public JsonResult Start(string email)
        {
            try
            {
                MailHelper.SendMail("chuongnh.hcm@gmail.com", email, "[ULI] thông báo", "Bạn vừa đăng ký nhận thông báo của ULI.");

                return Json(new { status = true, message = email }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = true, message = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}