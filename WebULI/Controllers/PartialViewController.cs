﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebULI.Models;

namespace WebULI.Controllers
{
    public class PartialViewController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PartialView
        [ChildActionOnly]
        public PartialViewResult _Topbar()
        {
            return PartialView();
        }
        // GET: PartialView
        [ChildActionOnly]
        public PartialViewResult _Header()
        {
            return PartialView();
        }
        // GET: PartialView
        [ChildActionOnly]
        public PartialViewResult  _Slider()
        {
            ViewBag.Sliders = db.Sliders.Where(x => x.ShowOnHome == true).ToList();
            return PartialView();
        }

        // GET: PartialView
        [ChildActionOnly]
        public PartialViewResult _Footer()
        {
            return PartialView();
        }

        // GET: PartialView
        [ChildActionOnly]
        public PartialViewResult _Sidebar()
        {
            return PartialView();
        }
    }
}