﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebULI.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using ledsangviet.com.CodeHelper;

namespace WebULI.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Slider
        public async Task<ActionResult> Index()
        {
            var applicationSliders = db.Sliders.Include(a => a.User);
            return View(await applicationSliders.ToListAsync());
        }

        // GET: Admin/Slider/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationSlider applicationSlider = await db.Sliders.FindAsync(id);
            if (applicationSlider == null)
            {
                return HttpNotFound();
            }
            return View(applicationSlider);
        }

        // GET: Admin/Slider/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Slider/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ImageUrl,Title,ShowOnHome,UpdateTime")] ApplicationSlider applicationSlider)
        {
            if (ModelState.IsValid)
            {
                applicationSlider.UserId = User.Identity.GetUserId();
                db.Sliders.Add(applicationSlider);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(applicationSlider);
        }

        // GET: Admin/Slider/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationSlider applicationSlider = await db.Sliders.FindAsync(id);
            if (applicationSlider == null)
            {
                return HttpNotFound();
            }
            return View(applicationSlider);
        }

        // POST: Admin/Slider/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,ImageUrl,Title,ShowOnHome,UpdateTime")] ApplicationSlider applicationSlider)
        {
            if (ModelState.IsValid)
            {
                applicationSlider.UserId = User.Identity.GetUserId();
                db.Entry(applicationSlider).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(applicationSlider);
        }

        // GET: Admin/Slider/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationSlider applicationSlider = await db.Sliders.FindAsync(id);
            if (applicationSlider == null)
            {
                return HttpNotFound();
            }
            return View(applicationSlider);
        }

        // POST: Admin/Slider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ApplicationSlider applicationSlider = await db.Sliders.FindAsync(id);
            db.Sliders.Remove(applicationSlider);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<JsonResult> ChangeImage(Guid? id)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["image"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    try
                    {
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                        try
                        {
                            string _FileName = GuidComb.GenerateComb() + Path.GetExtension(file.FileName);
                            if (Directory.Exists(Server.MapPath("~/Uploads")) == false)
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Uploads"));
                            }
                            string _path = Path.Combine(Server.MapPath("~/Uploads"), _FileName);
                            // lưu file và server
                            file.SaveAs(_path);

                            var model = await db.Sliders.FindAsync(id);
                            if (model != null)
                            {
                                // Xóa file cũ
                                string del_file = Path.Combine(Server.MapPath(model.ImageUrl));
                                if (System.IO.File.Exists(del_file))
                                {
                                    System.IO.File.Delete(del_file);
                                }
                                model.ImageUrl = "/Uploads/" + _FileName;
                                await db.SaveChangesAsync();
                                return Json(new { status = true, mes = "Thay đổi thành công" }, JsonRequestBehavior.AllowGet);
                            }
                            return Json(new { status = true, mes = "Thay đổi thất bại" }, JsonRequestBehavior.AllowGet);
                        }
                        catch { }
                    }
                    catch (Exception ex)
                    {
                        return Json(new { status = false, data = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { status = false, mes = "Có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
