﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebULI.Areas.Admin.Controllers
{
    [Authorize]
    public class PartialViewController : Controller
    {
        // GET: PartialView/_Header
        [ChildActionOnly]
        public PartialViewResult _Header()
        {
            return PartialView();
        }

        // GET: PartialView/_Sidebar
        [ChildActionOnly]
        public PartialViewResult _Sidebar()
        {
            return PartialView();
        }
    }
}