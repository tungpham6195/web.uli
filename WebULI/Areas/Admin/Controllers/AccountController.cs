﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebULI.Models;

namespace WebULI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AccountController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Admin/Account
        public ActionResult Index()
        {
            List<string> files = new List<string>();
            ViewBag.Users = db.Users.ToList();
            ViewBag.Roles = db.Roles.ToList();

            return View();
        }

        public async System.Threading.Tasks.Task<JsonResult> LockOut(string id, bool value)
        {
            var user = db.Users.Find(id);
            if (user != null)
            {
                user.LockoutEnabled = value;
                user.LockoutEndDateUtc = DateTime.Now.AddDays(15);
                await db.SaveChangesAsync();
                return Json(new { status = true, lockout = user.LockoutEnabled, data = user.LockoutEndDateUtc.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, lockout = false, data = "" }, JsonRequestBehavior.AllowGet);
        }
        public async System.Threading.Tasks.Task<JsonResult> Role(string id, string userid)
        {
            var user = db.Users.Find(userid);
            if (user != null)
            {
                // huy quyen
                if (user.Roles.Any(x => x.RoleId == id))
                {
                    user.Roles.Remove(user.Roles.FirstOrDefault(x => x.RoleId == id));

                    await db.SaveChangesAsync();
                    return Json(new { status = true, value = false, data = "Đã hủy quyền thành công." });
                }
                else
                {
                    user.Roles.Add(new IdentityUserRole
                    {
                        RoleId = id,
                        UserId = userid
                    });
                    await db.SaveChangesAsync();
                    return Json(new { status = true, value = true, data = "success" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = false, data = "error" }, JsonRequestBehavior.AllowGet);
        }
    }
}