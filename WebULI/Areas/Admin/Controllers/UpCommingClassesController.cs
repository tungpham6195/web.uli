﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebULI.Models;
using Microsoft.AspNet.Identity;

namespace WebULI.Areas.Admin.Controllers
{
    public class UpCommingClassesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/UpCommingClasses
        public async Task<ActionResult> Index()
        {
            var upCommingClasses = db.UpCommingClasses.Include(a => a.Course).Include(a => a.User);
            return View(await upCommingClasses.ToListAsync());
        }

        // GET: Admin/UpCommingClasses/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUpCommingClasses applicationUpCommingClasses = await db.UpCommingClasses.FindAsync(id);
            if (applicationUpCommingClasses == null)
            {
                return HttpNotFound();
            }
            return View(applicationUpCommingClasses);
        }

        // GET: Admin/UpCommingClasses/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name");
            return View();
        }

        // POST: Admin/UpCommingClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TecherName,UpCommingDate,ShowOnHome,CourseId")] ApplicationUpCommingClasses applicationUpCommingClasses)
        {
            if (ModelState.IsValid)
            {
                applicationUpCommingClasses.UserId = User.Identity.GetUserId();
                db.UpCommingClasses.Add(applicationUpCommingClasses);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", applicationUpCommingClasses.CourseId);
            return View(applicationUpCommingClasses);
        }

        // GET: Admin/UpCommingClasses/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUpCommingClasses applicationUpCommingClasses = await db.UpCommingClasses.FindAsync(id);
            if (applicationUpCommingClasses == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", applicationUpCommingClasses.CourseId);
            return View(applicationUpCommingClasses);
        }

        // POST: Admin/UpCommingClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,TecherName,UpCommingDate,ShowOnHome,CourseId")] ApplicationUpCommingClasses applicationUpCommingClasses)
        {
            if (ModelState.IsValid)
            {
                applicationUpCommingClasses.UpdateTime = DateTime.Now;
                applicationUpCommingClasses.UserId = User.Identity.GetUserId();
                db.Entry(applicationUpCommingClasses).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", applicationUpCommingClasses.CourseId);
            return View(applicationUpCommingClasses);
        }

        // GET: Admin/UpCommingClasses/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUpCommingClasses applicationUpCommingClasses = await db.UpCommingClasses.FindAsync(id);
            if (applicationUpCommingClasses == null)
            {
                return HttpNotFound();
            }
            return View(applicationUpCommingClasses);
        }

        // POST: Admin/UpCommingClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ApplicationUpCommingClasses applicationUpCommingClasses = await db.UpCommingClasses.FindAsync(id);
            db.UpCommingClasses.Remove(applicationUpCommingClasses);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
