﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using ledsangviet.com.CodeHelper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebULI.Models
{
    [Table("AspNetSlider")]
    public class ApplicationSlider
    {
        public ApplicationSlider()
        {
            UpdateTime = DateTime.Now;
            Id = GuidComb.GenerateComb();
        }
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Đường dẫn hình ảnh")]
        public string ImageUrl { get; set; }
        [Display(Name = "Tiêu đề bài viết")]
        public string Title { get; set; }
        [Display(Name = "Hiện trên trang chủ")]
        public bool ShowOnHome { get; set; }

        [Display(Name ="Ngày cập nhật")]
        public DateTime UpdateTime { get; set; }


        [Display(Name = "Người cập nhật")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }

    [Table("AspNetCourse")]
    public class ApplicationCourse
    {
        public ApplicationCourse()
        {
            UpdateTime = DateTime.Now;
            Id = GuidComb.GenerateComb();
            UpCommingClasses = new List<ApplicationUpCommingClasses>();
        }
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Đường dẫn hình ảnh")]
        public string ImageUrl { get; set; }

        [Display(Name = "Tên khóa học")]
        public string Name { get; set; }

        [Display(Name = "Chi tiết khóa học")]
        [AllowHtml]
        public string Content { get; set; }

        [Display(Name = "Mô tả khóa học")]
        public string Decsription { get; set; }

        [Display(Name = "Hiện trên trang chủ")]
        public bool ShowOnHome { get; set; }
        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "Người cập nhật")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public virtual IList<ApplicationUpCommingClasses> UpCommingClasses { get; set; }
    }

    [Table("AspNetCommingClasses")]
    public class ApplicationUpCommingClasses
    {
        public ApplicationUpCommingClasses()
        {
            UpdateTime = DateTime.Now;
            Id = GuidComb.GenerateComb();
        }
        [Key]
        public Guid Id { get; set; }

        [Display(Name ="Giảng viên")]
        public string TecherName { get; set; }

        [Display(Name = "Ngày khai giảng")]
        public string UpCommingDate { get; set; }

        [Display(Name = "Hiện trên trang chủ")]
        public bool ShowOnHome { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateTime { get; set; }


        [Display(Name = "Khóa học")]
        public Guid CourseId { get; set; }
        [ForeignKey("CourseId")]
        public virtual ApplicationCourse Course { get; set; }


        [Display(Name = "Người cập nhật")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }

    [Table("AspNetNews")]
    public class ApplicationNews
    {
        public ApplicationNews()
        {
            UpdateTime = DateTime.Now;
            Id = GuidComb.GenerateComb();
        }
        [Key]
        public Guid Id { get; set; }

        [Display(Name ="Đường dẫn hình ảnh")]
        public string ImageUrl { get; set; }

        [Display(Name = "Tiêu đề bài viết")]
        public string Title { get; set; }

        [Display(Name = "Nội dung bài viết")]
        [AllowHtml]
        public string Content { get; set; }

        [Display(Name ="Mô tả ngắn")]
        public string Decsription { get; set; }
        public bool ShowOnHome { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime UpdateTime { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }


    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual IList<ApplicationSlider> Sliders { get; set; }
        public virtual IList<ApplicationCourse> Courses { get; set; }
        public virtual IList<ApplicationUpCommingClasses> UpCommingClasses { get; set; }
        public virtual IList<ApplicationNews> News { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public virtual DbSet<ApplicationSlider> Sliders { get; set; }
        public virtual DbSet<ApplicationCourse> Courses { get; set; }
        public virtual DbSet<ApplicationUpCommingClasses> UpCommingClasses { get; set; }
        public virtual DbSet<ApplicationNews> News { get; set; }
    }
}