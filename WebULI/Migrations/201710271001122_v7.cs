namespace WebULI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetCourse", "Content", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetCourse", "Content");
        }
    }
}
