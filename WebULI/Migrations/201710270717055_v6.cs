namespace WebULI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetCourse", "UpdateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetCommingClasses", "UpdateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetNews", "UpdateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetSlider", "UpdateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetSlider", "UpdateTime");
            DropColumn("dbo.AspNetNews", "UpdateTime");
            DropColumn("dbo.AspNetCommingClasses", "UpdateTime");
            DropColumn("dbo.AspNetCourse", "UpdateTime");
        }
    }
}
