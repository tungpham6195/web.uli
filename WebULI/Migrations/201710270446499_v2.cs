namespace WebULI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetCourse", "ShowOnHome", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetCommingClasses", "ShowOnHome", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetNews", "ShowOnHome", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetSlider", "ShowOnHome", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetSlider", "ShowOnHome");
            DropColumn("dbo.AspNetNews", "ShowOnHome");
            DropColumn("dbo.AspNetCommingClasses", "ShowOnHome");
            DropColumn("dbo.AspNetCourse", "ShowOnHome");
        }
    }
}
