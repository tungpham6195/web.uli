namespace WebULI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetCommingClasses", "ImageUrl");
            DropColumn("dbo.AspNetCommingClasses", "Name");
            DropColumn("dbo.AspNetCommingClasses", "Decsription");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetCommingClasses", "Decsription", c => c.String());
            AddColumn("dbo.AspNetCommingClasses", "Name", c => c.String());
            AddColumn("dbo.AspNetCommingClasses", "ImageUrl", c => c.String());
        }
    }
}
